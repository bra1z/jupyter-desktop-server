# Jupyter Desktop Server

Run XFCE (or other desktop environments) on a JupyterHub.

This is based on https://github.com/ryanlovett/nbnovnc and a fork of https://github.com/manics/jupyter-omeroanalysis-desktop
